class bigfix::client($sugroup = undef){

# Is the SUGroup defined?
  if $sugroup == undef {
    fail('SUGroup is undefined, make sure you set the SUGroup')
  }

# BigFix doesn't make this dir (even at install) so we make it here
  file{'/etc/opt/BESClient':
    ensure => directory,
    before => File['/etc/opt/BESClient/actionsite.afxm'],
  }

# BigFix makes this folder at install time, but we need to pre-populate it, so we make it here
  file{'/var/opt/BESClient':
    ensure => directory,
    before => File['/etc/opt/BESClient/actionsite.afxm'],
  }

# Put the actionsite/masthead file in place
  file{'/etc/opt/BESClient/actionsite.afxm':
    ensure  => present,
    content => template("bigfix/etc/opt/BESClient/actionsite.afxm.erb"),
    before  => Package['besagent'],
  }

# Add SUGroup into the besclient.conf file
# NOTE: This isn't my favorite way of making this work, so if anyone reading this wants
# to help with converting this to use augeas, let ISO know
#
  exec { 'sugroup' :
    path    => [ '/usr/bin' , '/bin' ],
    unless  => "cat /var/opt/BESClient/besclient.config | grep $sugroup", #>/dev/null 2>&1",
    command => "printf '\n[Software\\BigFix\\EnterpriseClient\\Settings\\Client\\SUGroup]\nvalue = ${sugroup}\neffective date = Fri,%%2025%%20Oct%%202013%%2015:57:00%%20-0700' >> /var/opt/BESClient/besclient.config",
    notify  => Service['besclient'],
  }

# Get the correct installer from puppet
  case $::operatingsystem {
    centos, redhat, oraclelinux: {
      $bes_package_path = "puppet:///modules/bigfix/rhel/BESAgent-9.2.6.94-rhe5.x86_64.rpm"
      $bes_package = "BESAgent-9.2.6.94-rhe5.x86_64.rpm"
      $bes_name = "BESAgent-9.2.6.94-rhe5.x86_64"
      $provider = 'rpm'
      }
    debian, ubuntu: {
      $bes_package_path = "puppet:///modules/bigfix/debian/BESAgent-9.2.6.94-debian6.amd64.deb"
      $bes_package = "BESAgent-9.2.6.94-debian6.amd64.deb"
      $bes_name = "besagent"
      $provider = 'dpkg'
    }
  }

  file { "/etc/opt/BESClient/${bes_package}" :
    ensure => present,
    source => $bes_package_path,
    before => Package['besagent'],
  }

# Install besagent
  package{'besagent':
    ensure   => present,
    source   => "/etc/opt/BESClient/${bes_package}",
    name     => $bes_name,
    provider => $provider,
  }

# Run the service
  service{'besclient':
    ensure  => running,
    require => Package['besagent'],
  }

}
